﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class UnknownErrorDetector : MonoBehaviour
{
    [SerializeField] GameObject presenceFailure;
    [SerializeField] GameObject unknownFailure;
    private void Update()
    {
        if (StateManager.currentState == StateManager.machineState.PresenceFailure)
        {
            if (TobiiAPI.GetUserPresence() == UserPresence.Unknown)
            {
                unknownFailure.SetActive(true);
                presenceFailure.SetActive(false);
            }
            else if (TobiiAPI.GetUserPresence() == UserPresence.NotPresent)
            {
                unknownFailure.SetActive(false);
                presenceFailure.SetActive(true);
            }
        }
    }
}
