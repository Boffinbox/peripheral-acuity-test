﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tobii.Gaming;

public class StatsReporter : MonoBehaviour
{
    Vector3 position;
    Quaternion rotation;
    float depth;
    float yRotation;
    float width;
    float height;
    [SerializeField] TextMeshProUGUI distanceReporter;
    [SerializeField] TextMeshProUGUI yRotationReporter;
    [SerializeField] TextMeshProUGUI screenWidthReporter;
    [SerializeField] TextMeshProUGUI screenHeightReporter;
    [SerializeField] TextMeshProUGUI rollingMScoreReporter;
    void Start()
    {
    }
    void Update()
    {
        if (TobiiAPI.GetHeadPose().IsValid)
        {
            position = TobiiAPI.GetHeadPose().Position;
            rotation = TobiiAPI.GetHeadPose().Rotation;
            depth = position.magnitude;
            yRotation = rotation.eulerAngles.y;
            distanceReporter.text = depth.ToString();
            yRotationReporter.text = yRotation.ToString();
        }
        if (TobiiAPI.GetDisplayInfo().IsValid)
        {
            width = TobiiAPI.GetDisplayInfo().DisplayWidthMm;
            height = TobiiAPI.GetDisplayInfo().DisplayHeightMm;
            screenWidthReporter.text = width.ToString();
            screenHeightReporter.text = height.ToString();
        }
        rollingMScoreReporter.text = MScoreCalculator.MScore.ToString();
    }
}
