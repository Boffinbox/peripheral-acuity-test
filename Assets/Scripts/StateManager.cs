﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using Tobii.Gaming;

public class StateManager : MonoBehaviour
{
    public enum machineState { WelcomeScreen, ScreenSizeSetup, ScreenSize, InfoScreenSetup, InfoScreen, InitializeTest, PeripheralTest, FinishedTesting, Results, PresenceFailure }
    public static machineState currentState;
    machineState savedState;

    OptotypeCalculator optoCalc;
    [SerializeField] GameObject scalingRig;
    [SerializeField] MovingOptotype movingOptotype;
    [SerializeField] OptotypeShuffler optotypeShuffler;
    [SerializeField] GameObject presenceCanvas;
    [SerializeField] MovingOptotype demoOptotype;
    [SerializeField] GameObject demoRig;
    private void Awake()
    {
        TobiiAPI.GetGazePoint();
    }
    private void Start()
    {
        optoCalc = GetComponent<OptotypeCalculator>();
        currentState = machineState.WelcomeScreen;
        savedState = machineState.WelcomeScreen;
    }
    private void Update()
    {
        if (currentState != machineState.PresenceFailure)
        {
            if (TobiiAPI.GetUserPresence() == UserPresence.Unknown)
            {
                Debug.Log("presence error");
                Time.timeScale = 0;
                presenceCanvas.SetActive(true);
                currentState = machineState.PresenceFailure;
            }
            else if (TobiiAPI.GetUserPresence() == UserPresence.NotPresent)
            {
                Debug.Log("no presence detected");
                Time.timeScale = 0;
                presenceCanvas.SetActive(true);
                currentState = machineState.PresenceFailure;
            }
        }
        if (currentState == machineState.PresenceFailure)
        {
            if (TobiiAPI.GetUserPresence() == UserPresence.Present)
            {
                Debug.Log("presence detected: restoring " + savedState.ToString());
                Time.timeScale = 1;
                presenceCanvas.SetActive(false);
                currentState = savedState;
            }
        }
        else
        {
            if (currentState == machineState.WelcomeScreen)
            {
                WelcomeScreenRoutine();
            }
            if (currentState == machineState.ScreenSizeSetup)
            {
            }
            if (currentState == machineState.ScreenSize)
            {
            }
            if (currentState == machineState.InfoScreenSetup)
            {
                DemoVoiceRecognitionStartup();
                InfoScreenSetup();
                currentState = machineState.InfoScreen;
            }
            if (currentState == machineState.InfoScreen)
            {
            }
            if (currentState == machineState.InitializeTest)
            {
                VoiceRecognitionStartup();
                scalingRig.SetActive(true);
                currentState = machineState.PeripheralTest;
            }
            if (currentState == machineState.PeripheralTest)
            {
                PeripheralTestRoutine();
            }
            if (currentState == machineState.FinishedTesting)
            {
                FinishedTestingRoutine();
                currentState = machineState.Results;
            }
            if (currentState == machineState.Results)
            {
                ResultsRoutine();
            }
            savedState = currentState;
        }
    }
    [SerializeField] GameObject startScreenUI;
    void WelcomeScreenRoutine()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            currentState = machineState.ScreenSizeSetup;
            startScreenUI.SetActive(false);
            screenSizeUI.SetActive(true);
        }
    }
    [SerializeField] GameObject screenSizeUI;
    [SerializeField] TMP_InputField screenWidthInput;
    public void ScreenSizeSubmitted()
    {
        if (int.TryParse(screenWidthInput.text, out int i))
        {
            if (i <= 5000)
            {
                if (i >= 10)
                {
                    optoCalc.PhysicalWidth = i;
                    optoCalc.ScalingRigPositioner();
                    demoRig.GetComponent<OptotypeCalculator>().PhysicalWidth = i;
                    demoRig.GetComponent<OptotypeCalculator>().ScalingRigPositioner();
                    currentState = machineState.InfoScreenSetup;
                }
                else
                {
                    Debug.Log("screen size can't be below 10mm");
                }
            }
            else
            {
                Debug.Log("screen size can't be bigger than 5 meters");
            }
        }
        else
        {
            Debug.Log("invalid parse");
        }
    }
    void InfoScreenSetup()
    {
        currentState = machineState.InfoScreen;
        screenSizeUI.SetActive(false);
        infoScreenUI.SetActive(true);
        demoRig.SetActive(true);
    }
    [SerializeField] GameObject infoScreenUI;
    public void InitializeTheTest()
    {
        infoScreenUI.SetActive(false);
        demoRig.SetActive(false);
        demoKeywordRecognizer.Stop();
        demoKeywordRecognizer.Dispose();
        demoKeywords = null;
        currentState = machineState.InitializeTest;
    }
    void PeripheralTestRoutine()
    {
        if (CountOfRemainingOptotypes() <= 0)
        {
            currentState = machineState.FinishedTesting;
        }
    }
    [SerializeField] GameObject resultsUI;
    void FinishedTestingRoutine()
    {
        scalingRig.SetActive(false);
        resultsUI.SetActive(true);
    }
    void ResultsRoutine()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            ResetTheTest();
        }
    }
    void ResetTheTest()
    {
        SceneManager.LoadScene(0);
    }
    int CountOfRemainingOptotypes()
    {
        int count = 0;
        foreach (Transform a in optotypeShuffler.transform)
        {
            count += 1;
        }
        return count;
    }

    // The following methods and fields are part of voice recognition logic, adapted from Paolo Abela, https://www.youtube.com/watch?v=HwT6QyOA80E
    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    private void VoiceRecognitionStartup()
    {
        foreach (Transform a in optotypeShuffler.transform)
        {
            keywords.Add(a.GetComponentInChildren<TextMeshPro>().text, () =>
            {
                RecognizedOptotype(a.GetComponentInChildren<TextMeshPro>().text);
            });
        }

        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray(), ConfidenceLevel.Low);
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keywordRecognizer.Start();
    }
    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;

        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    void RecognizedOptotype(string letter)
    {
        Debug.Log(letter);
        if (letter == optotypeShuffler.CurrentOptotype.GetComponentInChildren<TextMeshPro>().text)
        {
            movingOptotype.ResetPosition();
            optotypeShuffler.ChooseNextOptotype();
            MScoreCalculator.CalculateM(optoCalc);
        }
        else
        {
            movingOptotype.ResetPosition();
            optotypeShuffler.ChooseNextOptotype();
            Debug.Log("incorrect identification");
        }
    }

    KeywordRecognizer demoKeywordRecognizer;
    Dictionary<string, System.Action> demoKeywords = new Dictionary<string, System.Action>();
    private void DemoVoiceRecognitionStartup()
    {
        demoKeywords.Add("c", () =>
        {
            ResetDemoOptotype();
        });
        demoKeywords.Add("start", () =>
        {
            InitializeTheTest();
        });
        demoKeywordRecognizer = new KeywordRecognizer(demoKeywords.Keys.ToArray(), ConfidenceLevel.Low);
        demoKeywordRecognizer.OnPhraseRecognized += DemoKeywordRecognizerOnPhraseRecognized;
        demoKeywordRecognizer.Start();
    }
    void DemoKeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;

        if (demoKeywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    void ResetDemoOptotype()
    {
        demoOptotype.ResetPosition();
    }
}
