﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MScoreCalculator
{
    public static float MScore { get; private set; }
    static int optotypeCount;
    public static void CalculateM(OptotypeCalculator optoCalc)
    {
        if (optoCalc != null)
        {
            float tantheta = ((optoCalc.OptoDistance / 2f) / 2000);
            float arctan = Mathf.Rad2Deg * Mathf.Atan(tantheta);
            float theta = arctan * 2;

            float fVaMar = 1; // this will remain fixed until i potentially add a foveal visual acuity checker.
            float pVaMar = 10; // it's 10, because that's the MAR of a LogMAR 1.0 optotype.

            // we're gonna take a value of 1 arcminute for fVaMar as an average value for calculating slope.
            // this will cut development time in the short term, at the cost of final m value accuracy.
            // calculating VA MAR needs an initial eye test, which is kinda difficult to do with pixels.

            MScoreRollingAverage(Mathf.Abs(pVaMar - fVaMar) / theta);
        }
    }
    static void MScoreRollingAverage (float newScoreToAdd)
    {
        float a = MScore * optotypeCount;
        a += newScoreToAdd;
        optotypeCount++;
        MScore = a / optotypeCount;
    }
}
