﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MScoreDisplayer : MonoBehaviour
{
    private void Start()
    {
        GetComponent<TextMeshProUGUI>().text = MScoreCalculator.MScore.ToString("#.###");
    }
}
