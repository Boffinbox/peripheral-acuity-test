﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;
using TMPro;

public class GetScreenSizeForEstimate : MonoBehaviour
{
    private void Start()
    {
        GetComponent<TMP_InputField>().text = TobiiAPI.GetDisplayInfo().DisplayWidthMm.ToString();
    }
}
