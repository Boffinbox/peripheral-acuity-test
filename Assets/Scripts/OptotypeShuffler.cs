﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptotypeShuffler : MonoBehaviour
{
    [SerializeField] GameObject testedOptotypeHolder;
    public GameObject CurrentOptotype { get; private set; }

    private void Start()
    {
        ChooseNextOptotype();
    }
    public void ChooseNextOptotype()
    {
        if (CurrentOptotype != null)
        {
            CurrentOptotype.transform.parent = testedOptotypeHolder.transform;
            CurrentOptotype.SetActive(false);
        }
        List<GameObject> listOfOptotypes = new List<GameObject>();
        foreach (Transform a in transform)
        {
            a.gameObject.SetActive(false);
            listOfOptotypes.Add(a.gameObject);
        }
        if (listOfOptotypes.Count > 0)
        {
            CurrentOptotype = listOfOptotypes[Random.Range(0, listOfOptotypes.Count)];
            CurrentOptotype.SetActive(true);
        }
    }
}
