﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingOptotype : MonoBehaviour
{
    [SerializeField] GameObject scalingRig;
    public int resetCount;
    private void OnEnable()
    {
        ResetPosition();
        resetCount = 0;
    }
    void Update()
    {
        MoveLeft(Vector3.Distance(scalingRig.transform.position, transform.position) / 10);
    }
    private void MoveLeft(float speed)
    {
        transform.localPosition = new Vector3(transform.localPosition.x - (speed * Time.deltaTime), transform.localPosition.y, transform.localPosition.z);
    }
    public void ResetPosition()
    {
        Vector3 temp = Camera.main.ViewportToWorldPoint(new Vector2(1.05f, 0.5f));
        transform.position = new Vector3(temp.x, scalingRig.transform.position.y, scalingRig.transform.position.z);
        resetCount++;
    }
}
