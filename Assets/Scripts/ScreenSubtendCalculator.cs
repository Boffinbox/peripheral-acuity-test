﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tobii.Gaming;
using System;

public class ScreenSubtendCalculator : MonoBehaviour
{
    [SerializeField] GameObject ULTarget;
    [SerializeField] GameObject LTarget;
    [SerializeField] GameObject LLTarget;
    [SerializeField] GameObject MTarget;
    [SerializeField] GameObject URTarget;
    [SerializeField] GameObject RTarget;
    [SerializeField] GameObject LRTarget;
    [SerializeField] TextMeshProUGUI screenWidthReporter;
    class GazeCubeData
    {
        public Vector3 storedRotation;
        public Vector3 storedPosition;
    }
    GazeCubeData ULData = new GazeCubeData();
    GazeCubeData LData = new GazeCubeData();
    GazeCubeData LLData = new GazeCubeData();
    GazeCubeData MData = new GazeCubeData();
    GazeCubeData URData = new GazeCubeData();
    GazeCubeData RData = new GazeCubeData();
    GazeCubeData LRData = new GazeCubeData();
    List<GameObject> gazeObjects;
    float yCheckTimer;
    Vector3 rotation;
    bool isScreenSizeCalculated = false;

    // Temporary debug stat reporters - these need to be removed before submission
    [SerializeField] TextMeshProUGUI ULRotText;
    [SerializeField] TextMeshProUGUI ULPosText;
    [SerializeField] TextMeshProUGUI LRotText;
    [SerializeField] TextMeshProUGUI LPosText;
    [SerializeField] TextMeshProUGUI LLRotText;
    [SerializeField] TextMeshProUGUI LLPosText;
    [SerializeField] TextMeshProUGUI MRotText;
    [SerializeField] TextMeshProUGUI MPosText;
    [SerializeField] TextMeshProUGUI URRotText;
    [SerializeField] TextMeshProUGUI URPosText;
    [SerializeField] TextMeshProUGUI RRotText;
    [SerializeField] TextMeshProUGUI RPosText;
    [SerializeField] TextMeshProUGUI LRRotText;
    [SerializeField] TextMeshProUGUI LRPosText;
    void Start()
    {
        gazeObjects = new List<GameObject>();
        if (ULTarget != null)
        {
            gazeObjects.Add(ULTarget);
        }
        if (LTarget != null)
        {
            gazeObjects.Add(LTarget);
        }
        if (LLTarget != null)
        {
            gazeObjects.Add(LLTarget);
        }
        if (MTarget != null)
        {
            gazeObjects.Add(MTarget);
        }
        if (URTarget != null)
        {
            gazeObjects.Add(URTarget);
        }
        if (RTarget != null)
        {
            gazeObjects.Add(RTarget);
        }
        if (LRTarget != null)
        {
            gazeObjects.Add(LRTarget);
        }
    }
    void Update()
    {
        Vector3 rot = TobiiAPI.GetHeadPose().Rotation.eulerAngles;
        if (rot.x > 180)
        {
            rot.x -= 360;
        }
        if (rot.y > 180)
        {
            rot.y -= 360;
        }
        if (rot.z > 180)
        {
            rot.z -= 360;
        }
        foreach (GameObject a in gazeObjects)
        {
            if (a.GetComponent<GazeAware>().HasGazeFocus)
            {
                yCheckTimer -= Time.deltaTime;
                if (yCheckTimer <= 0)
                {
                    CheckCurrentRotationAgainstStoredRotation(rot, a);
                    yCheckTimer = 1;
                }
            }
        }
        ScreenSizeCalc();
    }

    void CheckCurrentRotationAgainstStoredRotation(Vector3 rotation, GameObject a)
    {
        if (rotation == this.rotation)
        {
            StoreRotation(rotation, a);
        }
        else
        {
            this.rotation = rotation;
        }
    }
    void StoreRotation(Vector3 rotation, GameObject a)
    {
        if (a != null)
        {
            if (a == ULTarget)
            {
                ULData.storedRotation = rotation;
                ULData.storedPosition = TobiiAPI.GetHeadPose().Position;
                ULRotText.text = ULData.storedRotation.ToString();
                ULPosText.text = ULData.storedPosition.ToString();
            }
            if (a == LTarget)
            {
                LData.storedRotation = rotation;
                LData.storedPosition = TobiiAPI.GetHeadPose().Position;
                LRotText.text = LData.storedRotation.ToString();
                LPosText.text = LData.storedPosition.ToString();
            }
            if (a == LLTarget)
            {
                LLData.storedRotation = rotation;
                LLData.storedPosition = TobiiAPI.GetHeadPose().Position;
                LLRotText.text = LLData.storedRotation.ToString();
                LLPosText.text = LLData.storedPosition.ToString();
            }
            if (a == MTarget)
            {
                MData.storedRotation = rotation;
                MData.storedPosition = TobiiAPI.GetHeadPose().Position;
                MRotText.text = MData.storedRotation.ToString();
                MPosText.text = MData.storedPosition.ToString();
            }
            if (a == URTarget)
            {
                URData.storedRotation = rotation;
                URData.storedPosition = TobiiAPI.GetHeadPose().Position;
                URRotText.text = URData.storedRotation.ToString();
                URPosText.text = URData.storedPosition.ToString();
            }
            if (a == RTarget)
            {
                RData.storedRotation = rotation;
                RData.storedPosition = TobiiAPI.GetHeadPose().Position;
                RRotText.text = RData.storedRotation.ToString();
                RPosText.text = RData.storedPosition.ToString();
            }
            if (a == LRTarget)
            {
                LRData.storedRotation = rotation;
                LRData.storedPosition = TobiiAPI.GetHeadPose().Position;
                LRRotText.text = LRData.storedRotation.ToString();
                LRPosText.text = LRData.storedPosition.ToString();
            }
        }
    }
    private void ScreenSizeCalc()
    {
        // As we get more data, the accuracy of these calculation increases

        // Average Distance Calculator
        float avgDistance = ((ULData.storedPosition.z + LData.storedPosition.z + LLData.storedPosition.z + MData.storedPosition.z + URData.storedPosition.z + RData.storedPosition.z + LRData.storedPosition.z) / 7);

        // Screen Width Calculations
        float ULURCalc = SolveTheTriangle(ULData.storedRotation.y, URData.storedRotation.y, avgDistance);
        float ULRCalc = SolveTheTriangle(ULData.storedRotation.y, RData.storedRotation.y, avgDistance);
        float ULLRCalc = SolveTheTriangle(ULData.storedRotation.y, LData.storedRotation.y, avgDistance);
        float LURCalc = SolveTheTriangle(LData.storedRotation.y, URData.storedRotation.y, avgDistance);
        float LRCalc = SolveTheTriangle(LData.storedRotation.y, RData.storedRotation.y, avgDistance);
        float LLRCalc = SolveTheTriangle(LData.storedRotation.y, LRData.storedRotation.y, avgDistance);
        float LLURCalc = SolveTheTriangle(LLData.storedRotation.y, URData.storedRotation.y, avgDistance);
        float LLR2Calc = SolveTheTriangle(LLData.storedRotation.y, RData.storedRotation.y, avgDistance);
        float LLLRCalc = SolveTheTriangle(LLData.storedRotation.y, LRData.storedRotation.y, avgDistance);

        float widthAverage = ((ULURCalc + ULRCalc + ULLRCalc + LURCalc + LRCalc + LLRCalc + LLURCalc + LLR2Calc + LLLRCalc) / 9);

        screenWidthReporter.text = widthAverage.ToString();
    }
    /// <summary>
    /// Returns an estimate for the screen size, by giving the head rotations of the user at two known points, and the user's distance from screen.
    /// </summary>
    /// <param name="rot1"></param>
    /// <param name="rot2">This is subtracted from rot1 to determine theta.</param>
    /// <param name="dist">This should be z from a storedPosition</param>
    /// <returns></returns>
    float SolveTheTriangle(float rot1, float rot2, float dist)
    {
        float theta = Mathf.Abs(rot1 - rot2);
        // we have theta, we have a right angle, and we have a known length. solve the triangle!
        float alpha = theta / 2;
        // 180 - 90 - alpha = beta, the 180 - 90 cancels out
        float beta = (90 - alpha);
        float sineBeta = Mathf.Sin(Mathf.Deg2Rad * beta);
        float bOverSineBeta = dist / sineBeta;
        float sineAlpha = Mathf.Sin(Mathf.Deg2Rad * alpha);
        float unknownSide = bOverSineBeta * sineAlpha;
        return unknownSide * 2;
    }
}
