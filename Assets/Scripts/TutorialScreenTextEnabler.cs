﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScreenTextEnabler : MonoBehaviour
{
    [SerializeField] GameObject text1;
    [SerializeField] GameObject text2;
    [SerializeField] GameObject text3;
    private void OnEnable()
    {
        text1.SetActive(true);
    }
    private void Update()
    {
        if (GetComponent<MovingOptotype>().resetCount == 1)
        {
            text1.SetActive(false);
            text2.SetActive(true);
        }
        else if (GetComponent<MovingOptotype>().resetCount == 2)
        {
            text2.SetActive(false);
            text3.SetActive(true);
        }
        else if (GetComponent<MovingOptotype>().resetCount == 3)
        {
            Destroy(this);
        }
    }
}
