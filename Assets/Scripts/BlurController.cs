﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using Tobii.Gaming;

public class BlurController : MonoBehaviour
{
    [SerializeField] GameObject ducky;
    [SerializeField] GameObject moving;
    PostProcessVolume blurrer;
    private void Start()
    {
        blurrer = GetComponent<PostProcessVolume>();
    }
    private void Update()
    {
        if (StateManager.currentState == StateManager.machineState.PeripheralTest)
        {
            float duckyX = ducky.transform.position.x;
            float optoX = moving.transform.position.x;
            float difference = Mathf.Abs(optoX - duckyX);
            Vector2 gazePoint = Camera.main.ViewportToWorldPoint(TobiiAPI.GetGazePoint().Viewport);
            float gazeDifference = Mathf.Abs(gazePoint.x - duckyX);
            blurrer.weight = (gazeDifference / difference) * 1.5f;
        }
        else if (StateManager.currentState == StateManager.machineState.PresenceFailure)
        {
            blurrer.weight = 1;
        }
        else
        {
            blurrer.weight = 0;
        }
    }
}
