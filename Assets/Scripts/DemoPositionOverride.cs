﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoPositionOverride : MonoBehaviour
{
    private void Start()
    {
        Vector3 temp = Camera.main.ViewportToWorldPoint(new Vector2(1.4f, 0.5f));
        transform.position = new Vector3(temp.x, transform.position.y, transform.position.z);
    }
}
