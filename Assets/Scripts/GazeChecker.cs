﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class GazeChecker : MonoBehaviour
{
    List<GameObject> gazeObjects;
    // Start is called before the first frame update
    void Start()
    {
        gazeObjects = new List<GameObject>();
        gazeObjects.AddRange(GameObject.FindGameObjectsWithTag("GazeObject"));
    }

    // Update is called once per frame
    void Update()
    {
        CheckObjects(gazeObjects);
    }
    void CheckObjects(List<GameObject> gazeObjects)
    {
        foreach (GameObject a in gazeObjects)
        if (a.GetComponent<GazeAware>().HasGazeFocus)
        {
            ChangeColor(a, Color.grey);
        }
        else
        {
            ChangeColor(a, Color.black);
        }
    }
    void ChangeColor(GameObject target, Color color)
    {
        target.GetComponent<Renderer>().material.color = color;
    }
}
