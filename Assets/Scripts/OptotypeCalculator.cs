﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;
using TMPro;

public class OptotypeCalculator : MonoBehaviour
{
    float virtualWidth;
    float physicalPixelSize;
    float optotypePixelCount;
    public float OptoDistance { get; private set; }
    public float PhysicalWidth { get; set; } = 10;

    [SerializeField] GameObject scalingRig;
    [SerializeField] GameObject movingOptotype;
    void Update()
    {
        Vector2 topPos = new Vector2(Screen.width / 2, (Screen.height + optotypePixelCount) / 2);
        Vector2 bottomPos = new Vector2(Screen.width / 2, (Screen.height - optotypePixelCount) / 2);
        topPos = Camera.main.ScreenToWorldPoint(topPos);
        bottomPos = Camera.main.ScreenToWorldPoint(bottomPos);
        float initialScaleFactor = Vector2.Distance(topPos, bottomPos);
        if (TobiiAPI.GetHeadPose().IsValid)
        {
            float modifiedScaleFactor = initialScaleFactor * (TobiiAPI.GetHeadPose().Position.z / 2000);
            // incorporating the current scale and blending it with the last scale allows for a smoother transitions between scales
            modifiedScaleFactor = (modifiedScaleFactor + (scalingRig.transform.localScale.x)) / 2;
            ScaleTheRig(modifiedScaleFactor);
        }
        else
        {
            ScaleTheRig(initialScaleFactor);
            // optotype.transform.localScale = new Vector3(initialScaleFactor, initialScaleFactor, initialScaleFactor);
            // optotypeTest.transform.localScale = new Vector3(initialScaleFactor, initialScaleFactor, initialScaleFactor);
        }
        Vector2 pos1 = Camera.main.WorldToScreenPoint(scalingRig.transform.position);
        Vector2 pos2 = Camera.main.WorldToScreenPoint(movingOptotype.transform.position);
        float optoPixelDistance = Vector2.Distance(pos1, pos2);
        // convert pixels to millimeters
        float optoPhysicalScreenDistance = optoPixelDistance * physicalPixelSize;
        // normalise this distance to 2 meters. this means that the distance being recorded is equivalent to a 2m physical test
        OptoDistance = (optoPhysicalScreenDistance * (2000 / TobiiAPI.GetHeadPose().Position.z));
    }
    private void ScaleTheRig(float scalingFactor = 1f)
    {
        scalingRig.transform.localScale = new Vector3(scalingFactor, scalingFactor, scalingFactor);
    }
    public void ScalingRigPositioner()
    {
        virtualWidth = Screen.currentResolution.width;
        physicalPixelSize = PhysicalWidth / virtualWidth;
        // Our floating optotype must have a physical size of 29.0908721886555mm to represent a 2000mm distance, at LogMAR 1.0
        optotypePixelCount = Mathf.Round(29.091f / physicalPixelSize);
        //Debug.Log(optotypePixelCount); // expecting a value around 162
        Vector3 temp = Camera.main.ViewportToWorldPoint(new Vector2(0.05f, 0.5f));
        scalingRig.transform.position = new Vector3(temp.x, scalingRig.transform.position.y, scalingRig.transform.position.z);
    }
}
