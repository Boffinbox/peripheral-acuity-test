﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingUIElement : MonoBehaviour
{
    Graphic element;
    enum FadeState { Out, In, Fading }
    FadeState state;
    void Start()
    {
        state = FadeState.In;
        element = GetComponent<Graphic>();
    }
    private void Update()
    {
        if (state == FadeState.Out)
        {
            StartCoroutine(FadeIn());
            state = FadeState.Fading;
        }
        else if (state == FadeState.In)
        {
            StartCoroutine(FadeOut());
            state = FadeState.Fading;
        }
    }
    IEnumerator FadeOut()
    {
        while (element.color.a > 0)
        {
            element.color = new Color(element.color.r, element.color.g, element.color.b, element.color.a - 0.01f);
            yield return null;
        }
        state = FadeState.Out;
        yield return null;
    }
    IEnumerator FadeIn()
    {
        while (element.color.a < 1)
        {
            element.color = new Color(element.color.r, element.color.g, element.color.b, element.color.a + 0.01f);
            yield return null;
        }
        state = FadeState.In;
        yield return null;
    }
}
